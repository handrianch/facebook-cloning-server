var express = require('express');
var router  = express.Router();
var posts   = require('../models').post
var user    = require('../models').user

router.get('/', function(req, res, next) {
	posts.findAll({
			include: 'user',
			order: [
				['createdAt', 'DESC']
			]
		})
		.then(data => {
			data = data.map(item => {
				delete item.user.dataValues.password
		 		return item;
		 	});
		 	res.status(200).json(data);
		})
		.catch(err => {
		 	res.status(500).json({
		 		message: "server error",
		 	})
		})
});

router.post('/', function(req, res, next) {
	let body = {
		posts: req.body.posts,
		user_id: req.user.id
	}

	posts.create(body)
		 .then(data => {
		 	res.status(201).json(data.dataValues);
		 })
		 .catch(err => {
		 	res.status(500).json({
		 		message: "server error",
		 	})
		 })
})

router.get('/:id', function(req, res, next) {
	posts.findByPk(req.params.id, {include: ['user']})
		 .then(data => {
		 	if(data) {
			 	res.status(200).json(data.dataValues);
		 	} else {
		 		res.status(400).json({
		 			message: `post with id ${req.params.id} not found`,
		 		})
		 	}
		 })
		 .catch(err => {
		 	res.status(500).json({
		 		message: 'server error',
		 	})
		 })
});

router.patch('/:id', function(req, res, next) {
	let body = {
		posts: req.body.posts
	}
	posts.update(body, {where: {id: req.params.id, user_id: req.user.id}})
		 .then(data => {
		 	if(data[0]) {
		 		posts.findOne({where : {id: req.params.id}})
		 			 .then(data => res.status(200).json(data))
		 			 .catch(err => res.status(500).json({message: 'server error'}))
		 	} else {     
		 		res.status(403).json({
		 			message: 'unauthorized',
		 		})
		 	}
		 })
		 .catch(err => {
		 	res.status(500).json({
		 		message: 'server error',
		 	})
		 })
})

router.delete('/:id', function(req, res, next) {
	posts.destroy({where: {
		id: req.params.id,
		user_id: req.user.id
	}}).then(function(arg) {
		if(arg) {
			res.status(200).json({message: 'delete success'})
		} else {
			res.status(403).json({
	 			message: 'unauthorized',
	 		})
		}
	})
	   .catch(err => res.status(500).json({message: 'server error'}))
})

module.exports = router