var express = require('express');
var router  = express.Router();
var users 	= require('../models').user

/* GET users listing. */
router.get("/", function(req, res, next) {
	users.findAll()
		 .then(data => {
		 	data = data.map(item => {
				 		delete item.dataValues.password
				 		return item.dataValues;
				 	});
		 	res.json(data);
		 })
		 .catch(err => console.error(err));
});

router.get("/:id", function(req, res, next) {
	users.findByPk(req.params.id)
		 .then(data => {
		 	delete data.dataValues.password;
		 	res.json(data);
		 })
		 .catch(err => console.error(err));
});

module.exports = router;
