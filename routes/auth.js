const express 		= require('express');
const router  		= express.Router();
const jwt 	  		= require('jsonwebtoken');
const bcrypt  		= require('bcrypt');
const user 	  		= require('../models').user

router.post('/', function(req, res, next) {
	let identity = {
		email: req.body.email
	}

	user.findOne({where: identity})
		.then(data => {
			if(!(data)) {
				res.status(403).json({
					message: 'authentication failed',
					status: 403
				});
			} else {
				payload = data.dataValues;
				password = payload.password;

				delete payload.password;
				delete payload.createdAt;
				delete payload.updatedAt;

				bcrypt.compare(req.body.password, password, function(err, result) {
					if(!result) {
						return res.status(403).json({
							message: 'authentication failed',
							status: 403
						});
					}

					let token = jwt.sign(payload, process.env.SECRET_KEY, {expiresIn: process.env.EXPIRES_IN});
				
					return res.status(200).json({
								data: payload,
								token: token,
								status: 200
							});
				})
			}
		})
		.catch(err => res.status(500).send('Server error bre!'));
});

module.exports = router;