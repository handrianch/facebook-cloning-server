const express = require('express'),
	  router  = express.Router();

router.get('/', function(req, res, next) {
	delete req.user.email;
	res.json(req.user);
});

module.exports = router;