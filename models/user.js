'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar: DataTypes.TEXT,
  }, {});
  user.associate = function(models) {
    // associations can be defined here
    user.hasMany(models.post, {
    	foreignKey: 'user_id'
    })
  };
  return user;
};