'use strict';
module.exports = (sequelize, DataTypes) => {
  const post = sequelize.define('post', {
    posts: DataTypes.TEXT,
    user_id: DataTypes.INTEGER,
  }, {});
  post.associate = function(models) {
    // associations can be defined here
    post.belongsTo(models.user, {
    	foreignKey: 'user_id'
    });
  };
  return post;
};