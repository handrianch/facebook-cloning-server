'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [
      {
        name: 'saitama',
        email: 'saitama@email.com',
        password: '12345',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'fang',
        email: 'fang@email.com',
        password: '12345',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'edward',
        email: 'edward@email.com',
        password: '12345',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('users', null, {});
  }
};
